These are the hooks used by the sns system.


/**
* When a the system receives a message from SNS, this hook gets called in the receiving callback.
* Any module interested in acting on a message notification should implement this hook for processing.
* 
* @param $message is the message that gets passed back from Amazon it looks like this:
* $message = array(
*   'id' => 'Subject' // This gets set as the original subject line. We lose context of the orig id when coming back from amazon
*   'body' => 'The body',
*   'topic' => 'Topic where this message was published',
*   'original' => 'reference to the original return object from Amazon'
* );
*/
hook_sns_notification($message, $topic)


/**
* When sns_publish is called the system will give other modules the ability to alter anything 
* about the message before it gets sent. One example is the case of nodes. SNS has an 8k limit 
* and some nodes are > 8K especially when dealing with Many CCK fields or large bodies. The SNS 
* Module uses zlib to gzip the serialized node. It then runs it through base64 to make sure that
* all of the data conforms to utf-8 chars (to make SNS happy). This is all done in this alter function
* as to decouple it from the action of publishing the message in general. Other modules may have to 
* take a similar approach to make their messages fit through SNS
* 
* @param array() $message. The message structure that looks like:
* $message = array(
*   'id' => 'some ID' // Set by the caller of sns_publish,
*   'subject' => 'Message Subject' // Set by the caller of sns_publish,
*   'body' => 'Message Body'
* )
*/
hook_sns_publish_message_alter(&$message)

                            
/**
* By default the sns module uses nodeapi to respond to actions taken on nodes.
* The actions it acts upon are insert, update, and delete. If you want a particular
* node type pushed into to SNS, implement this hook. 
* 
* @return array('node_type' => 'sns_topic');
*/
hook_sns_publish_node_type()
