<?php


/**
* This is the SNS listener callback. 
* It will take the input pushed in from SNS and respond appropriately.
* If the message is a subscription request, it will automatically confirm 
* If the message is a notification it will parse the topic and call hook_sns_notification
*/
function sns_response_handler() {
  $response = file_get_contents('php://input');
  $resp = json_decode($response);
  
  if ($resp->Type == 'SubscriptionConfirmation') {
    sns_subscribe_confirmation($resp->TopicArn, $resp->Token);
  }
  else if ($resp->Type == 'Notification') {
    $message = array();
    // Decode the topic
    $parsed = explode(':', $resp->TopicArn);
    $resp->Topic = $parsed[5];
    $message['id'] = $resp->Subject;
    $message['body'] = $resp->Message;
    $message['topic'] = $resp->Topic;
    $message['original'] = $resp;
    
    /**
    * This hook must be called like since module_invoke_all uses call_user_func_args to execute hooks
    * this way ensures $message gets passed as a reference
    */
    foreach (module_implements('sns_notification') as $name) {
      $function = $name .'_sns_notification';
      $function(&$message, $message['topic']);
    }
  }
}

function sns_subscribe_confirmation($arn, $token) {
  $sns = new AmazonSNS();
  $resp = $sns->confirm_subscription($arn, $token);

  if (!$resp->isOK()) {
    watchdog('sns', 'SNS: Could not confirm subscription', NULL, WATCHDOG_ERROR);
  }
}