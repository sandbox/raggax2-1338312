This module is designed to provide a push notification system to Drupal. 

Per Amazon:
Amazon Simple Notification Service (Amazon SNS) is a web service that makes it easy to set up, operate, and send notifications from the cloud. It provides developers with a highly scalable, flexible, and cost-effective capability to publish messages from an application and immediately deliver them to subscribers or other applications. It is designed to make web-scale computing easier for developers.

Rationale: 
To use this module you need to install the Creeper module to provide the Amazon SDK for PHP. Currently, there is no set module for Amazon SNS for Drupal 6. While Amazon SDK for PHP provides an interesting possbility, it is currently only available for Drupal 7. Using a push notification system provides some interesting functionality for Drupal. Here are a couple of examples:

I am a Drupal developer / administrator and want to know if there are any critical watchdog errors happening on my site. I can create a "system" topic in SNS and subscribe my email and SMS to it. Everytime a critical error gets passed through watchdog, SNS will send me the watchdog message.

I run a network of ecommerce sites. I want to let site A know when a transaction occurs on  site B. Site B can push a message (say, serialized data) to SNS and the subscribed site (site A) can respond to that message by sending an email or recording a transaction.

API
There are 3 variables that can be set to enable watchdog notifications:
$conf['sns_enable_watchdog'] = TRUE;
$conf['sns_watchdog_types'] = array() ; // Per watchdog_serverity_levels(); Currenlty set to EMERG, ALERT, CRITICAL
$conf['sns_watchdog_channels'] => array(); // a list of ARNs to notify on watchdog notifications

One can simply create Topics in the admin interface and push notifications to them. 

If a developer wants to respond to a particular message, simply implement hook_sns_notifcation($message). In this case, $message is the object pushed through by Amazon.

