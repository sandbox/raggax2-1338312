<?php
/**
 * @file
 * Admin forms and page callbacks
 */

/**
 * Page Callback for general SNS admin landing page
 */
function sns_admin() {
  $topic_list = sns_get_topics(TRUE);

  $headers = array(
    t('Name'),
    t('Machine Name'),
    t('Subscriptions Pending'),
    t('Subscriptions Confirmed'),
    t('Actions'),
  );

  $table_data = array();
  foreach ($topic_list as $key => $val) {
    $table_data[] = array(
      $val['DisplayName'],
      $key,
      $val['SubscriptionsPending'],
      $val['SubscriptionsConfirmed'],
      implode(' | ', array(
        l(t('Show'), 'admin/build/sns/' . $key),
        l(t('Delete'), 'admin/build/sns/' . $key . '/delete'),
        l(t('Push Message'), 'admin/build/sns/'. $key . '/push'),
        l(t('Subscribe'), 'admin/build/sns/' . $key . '/subscribe'),
      ))
    );
  }

  $rtn = '<h2>'. t('Currently Active Topics') . '</h2>';
  $rtn .=  theme('table', $headers, $table_data);
  return $rtn;
}

function sns_admin_show_detail($id) {
  $sns = new AmazonSNS();
  $sub_data = array();
  $result = cache_get($id, 'cache_sns');
  $result = $result->data;

  // Get Subscriptions
  $results = $sns->list_subscriptions_by_topic($result['TopicArn']);

  $subs = $results->body->xpath('ListSubscriptionsByTopicResult/Subscriptions/member');
  foreach ($subs as $sub) {
    $arn = (string)$sub->SubscriptionArn;
    if (preg_match('/^arn.*/', $arn)) {
      $confirmed = t('Confirmed');
    }
    else {
      $confirmed = t('Pending Confirmation');
    }

    $sub_data[] = array(
      (string)$sub->Endpoint,
      $confirmed,
      (string)$sub->Protocol,
    );
  }

  $sub_headers = array(
    t('Endpoint'),
    t('Status'),
    t('Protocol'),
  );

  $rtn = '<h1>' . t('SNS Topic Details') . '</h1>';
  $rtn .= '<p><strong>' . t('Name') . ': </strong>' . $result['DisplayName'] . '</p>';
  $rtn .= '<p><strong>' . t('Subscriptions Confirmed') . ': </strong>' . $result['SubscriptionsConfirmed'] . '</p>';
  $rtn .= '<p><strong>' . t('Subscriptions Pending') . ': </strong>' . $result['SubscriptionsPending'] . '</p>';

  $rtn .= '<h2>' . t('Topic Subscriptions') . '</h2>';
  $rtn .= theme('table', $sub_headers, $sub_data);
  return $rtn;
}

/**
 * Page callback for deleting topics
 */
function sns_admin_delete($id) {
  return drupal_get_form('sns_topic_delete_form', $id);
}

/**
 * Page callback for publishing messages
 */
function sns_admin_push_msg($id) {
  return drupal_get_form('sns_message_form', $id);
}

/**
 * Form constructor for creating a form
 */
function sns_topic_form() {
  $form = array();
  $form['sns_machine_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Topic Machine Name',
    '#required' => TRUE,
    '#maxlength' => 255,
    '#description' => 'Must be only upper and lower case ASCII, numbers and hyphens',
  );

  $form['sns_display_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Display Name',
    '#required' => TRUE,
    '#maxlength' => 64,
    '#description' => 'Human readable name',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  $form['#redirect'] = 'admin/build/sns';

  return $form;
}

/**
 * Submit handler for creating a topic
 */
function sns_topic_form_submit($form, $form_state) {
  $values = $form_state['values'];

  $sns = new AmazonSNS();

  $result = $sns->createTopic($values['sns_machine_name']);
  if ($result->isOK()) {
    $arn = (string)$result->body->CreateTopicResult->TopicArn;
    // Set the DisplayName
    $sns->set_topic_attributes($arn, 'DisplayName', $values['sns_display_name']);
  }
  else {
    drupal_set_message('error', t('There was an error creating your topic'));
  }

  cache_clear_all(NULL, 'cache_sns');
}

/**
 * Form constructor for deleting a topic
 */
function sns_topic_delete_form($form, $id) {
  $form = array();

  $form['sns_machine_name'] = array(
    '#type' => 'hidden',
    '#value' => $id
  );

  $form = confirm_form(
    $form,
    t('Really Delete !key?', array('!key' => $id)),
    'admin/build/sns'
  );

  $form['#redirect'] = 'admin/build/sns';
  return $form;
}

/**
 * Submit Handler for the delete functionality
 */
function sns_topic_delete_form_submit($form, $form_state) {
  $sns = new AmazonSNS();
  $key = $form_state['values']['sns_machine_name'];
  $arn = _sns_get_arn($key);

  $result = $sns->delete_topic($arn);

  if ($result->isOK()) {
    drupal_set_message(t('Successfully deleted @id', array('@id' => $key)));
  }
  else {
    drupal_set_message(t('Error: could not delete @id', array('@id' => $key)), 'error');
  }

  cache_clear_all(NULL, 'cache_sns');
}

/**
 * Form constructor for the admin publish functionality
 */
function sns_message_form($form_state, $id) {
  $form = array();
  $form['subject'] = array(
    '#title' => t('Subject'),
    '#type' => 'textfield',
    '#maxsize' => 100,
    '#description' => '(optional) Used for email endpoints. Max 100 chars',
  );

  $form['message'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#description' => 'Body of the message. Max 8192 bytes',
  );

  $form['topic_key'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#redirect'] = 'admin/build/sns';

  return $form;
}

/**
 * Push message submit handler
 */
function sns_message_form_submit($form, $form_state) {
  $message = array();
  $values = $form_state['values'];
  $arn = _sns_get_arn($values['topic_key']);

  if (!empty($values['subject'])) {
    $message['subject'] = $values['subject'];
  }

  $message['body'] = $values['message'];
  $resp = sns_publish($arn, 'sns_admin', $message);

  if ($resp) {
    drupal_set_message(t('SNS: Message Successfully sent'));
  }
  else {
    drupal_set_message(t('SNS: Message could not be sent'), 'error');
  }
}

/**
 * Page handler for adding subscriptions
 */
function sns_admin_add_subscriber($key) {
  return drupal_get_form('sns_add_subscriber_form', $key);
}

function sns_add_subscriber_form($form_state, $key) {
  
  $form['arn_key'] = array(
    '#type' => 'hidden',
    '#value' => $key
  );
  
  $form['subscription_type'] = array(
    '#type' => 'select',
    '#title' => t('Subscription protocol'),
    '#options' => array(
      '' => t('- Select One -'),
      'http' => t('HTTP'),
      'https' => t('HTTPS'),
      'email' => t('Email'),
      'email-json' => t('Email JSON'),
      'sms' => t('SMS'),
      'sqs' => t('SQS'),
    ),
    '#required' => TRUE,
  );
  
  $form['subscription_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Subscription Endpoint'),
    '#description' => t('This can be different depending on the protocol specified'),
    '#maxlength' => '255',
    '#required' => TRUE,
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  
  $form['#redirect'] = 'admin/build/sns';
  return $form;
}

function sns_add_subscriber_form_submit($form, &$form_state) {
  $arn = _sns_get_arn($form_state['values']['arn_key']);
  $type = $form_state['values']['subscription_type'];
  $endpoint = $form_state['values']['subscription_endpoint'];
  $sns = new AmazonSNS();
  
  $sns->subscribe($arn, $type, $endpoint);

  drupal_set_message(t('!endpoint has been subscribed. Awaiting subscription confirmation', array('!endpoint' => $endpoint)));
  cache_clear_all(NULL, 'cache_sns');
}